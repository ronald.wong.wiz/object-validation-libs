# object-validation-libs

Object validation libraries research


## Description
Research for validation libraries to replace the old one (Forg.js)
- [validatorjs](https://github.com/mikeerickson/validatorjs) 
- [joi](https://github.com/sideway/joi) 

# How to start
1. `npm i`
2. `node forg.js`, reference the original test cases / rules
3. `node index.js` / `node joi.js` for validatorjs / joi

# Summary
Useful API tested on both libraries
- [x] Passing parameter to rules
- [x] Custom field name 
- [x] Custom error message
- [x] Type checking (eg. String / Integer / Boolean)
- [x] Format checking (eg. Email / Regex)
- [x] Other basic checking

For the type checking on boolean on validatorjs, it will also pass the validation on 'true', 'false', '0', '1', which is different from ForgJs and joi. Ref: https://github.com/mikeerickson/validatorjs#boolean
```
boolean
The field under validation must be a boolean value of the form true, false, 0, 1, 'true', 'false', '0', '1',
```



