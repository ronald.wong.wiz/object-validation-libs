export const NAME_MAX_LENGTH = 32;

export const MAC_ADDRESS_LENGTH = 12;

export const PASSWORD_MIN_LENGTH = 8;
export const PASSWORD_MAX_LENGTH = 40;
export const PASSWORD_UPPERCASE_MIN = 1;
export const PASSWORD_NUMBERS_MIN = 1;

export const Accessory = {
  Type: {
    REMOTE_CONTROL: 1,
    BLE_BUTTON: 2,
    PIR_SENSOR: 16,
    WI_FI_REMOTE_CONTROL: 42,
    WI_FI_PIR_SENSOR: 43,
  },
};
