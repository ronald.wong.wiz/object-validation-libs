const data = {
  name: "00000000001111111111222222222233",
  // name: "000000000011111111112222222222333", // Error: Max = 32
  mac: "12345678901a", // Correct
  // mac: "12345678901a1", // Error: length must be 12
  mesh: "123", // Correct
  // mesh: 123, // Error: Type must be string
  password: "12345678901234567890123456789012345678Ab", // Correct
  // password: "12345Ab", // Error: Min = 8
  // password: "1234567890123456789012345678901234567890Ab", // Error: Max = 40
  // password: "12345678AB", // Error: At least 1 upper case and 1 lower case and 1 digit
  accessoryType: 1, // Correct
  // accessoryType: 0, // Error: not in the options
  email: 'abc@example.com', // Correct
  // email: 'abc@example', // Error: Invalid email format
  status: true,
  // status: 'true', // Error: Type must be boolean
  // status: 1, // Error: Type must be boolean
};

export default data;