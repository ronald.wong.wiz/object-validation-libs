// Reference from setup backend  validations

import forgjs from "@cesium133/forgjs";
const { Validator, Rule } = forgjs;
import data from "./data.js";
import * as CONSTANT from "./constant.js";

const log = console.log;

const nameRule = new Rule(
  {
    type: "string",
    optional: false,
    notEmpty: true,
    maxLength: CONSTANT.NAME_MAX_LENGTH,
  },
  `"name" cannot be empty and cannot contain more than ${CONSTANT.NAME_MAX_LENGTH} characters`
);

const macRule = new Rule(
  {
    type: "string",
    optional: false,
    minLength: CONSTANT.MAC_ADDRESS_LENGTH,
    maxLength: CONSTANT.MAC_ADDRESS_LENGTH,
    match: /^[0-9a-f]{12}$/g,
  },
  '"mac" should be a valid MAC address'
);

const validatorStringRule = (fieldName, optional = false) => {
  return new Rule(
    {
      type: "string",
      optional,
    },
    `"${fieldName}" should be a valid string`
  );
};

const accessoryValidatorTypeRule = (optional = false) => {
  return new Rule(
    {
      type: "int",
      optional,
      oneOf: [
        CONSTANT.Accessory.Type.REMOTE_CONTROL,
        CONSTANT.Accessory.Type.BLE_BUTTON,
        CONSTANT.Accessory.Type.PIR_SENSOR,
        CONSTANT.Accessory.Type.WI_FI_REMOTE_CONTROL,
        CONSTANT.Accessory.Type.WI_FI_PIR_SENSOR,
      ],
    },
    "Accessory type error"
  );
};

const userValidatorPasswordRule = new Rule(
  {
    type: "password",
    minLength: CONSTANT.PASSWORD_MIN_LENGTH,
    maxLength: CONSTANT.PASSWORD_MAX_LENGTH,
    uppercase: CONSTANT.PASSWORD_UPPERCASE_MIN,
    numbers: CONSTANT.PASSWORD_NUMBERS_MIN,
  },
  `"password" should be between ${CONSTANT.PASSWORD_MIN_LENGTH} and ${CONSTANT.PASSWORD_MAX_LENGTH} characters long and contain at least ${CONSTANT.PASSWORD_UPPERCASE_MIN} capital letter and at least ${CONSTANT.PASSWORD_NUMBERS_MIN} digit`
);

const validatorEmailRule = (optional = false) => {
  return new Rule({
    type: "email",
    optional,
  }, "\"email\" should be a valid email address");
}

const validatorBooleanRule = (fieldName, optional = false) => {
  return new Rule({
    type: "boolean",
    optional,
  }, `"${fieldName}" should be a boolean value`);
}

const vComplex = new Validator({
  name: nameRule,
  mac: macRule,
  mesh: validatorStringRule("meshUuid", true),
  password: userValidatorPasswordRule,
  accessoryType: accessoryValidatorTypeRule(),
  email: validatorEmailRule(),
  status: validatorBooleanRule("status")
});

let result = vComplex.test(data);
log("pass?", result);
log(vComplex.getErrors(data));
