import chalk from "chalk";
import Validator from "validatorjs";
import _ from "lodash";
import data from "./data.js";
import * as CONSTANT from "./constant.js";

const log = console.log;

/** Construct the rule string */
const getRuleStr = (ruleObj) => {
  const obj = _.pickBy(ruleObj, (val) => val);
  const arr = _.keys(obj);
  const str = _.join(arr, "|");

  return str;
};

const nameRule = (optional = false) => {
  const rules = {
    string: true,
    [`max:${CONSTANT.NAME_MAX_LENGTH}`]: true,
    required: optional,
  };

  return getRuleStr(rules);
};

const macRule = (optional = false) => {
  const rules = {
    string: true,
    required: optional,
    "regex:/^[0-9a-f]*$/g": true,
  };

  rules[`max:${CONSTANT.MAC_ADDRESS_LENGTH}`] = true;
  rules[`min:${CONSTANT.MAC_ADDRESS_LENGTH}`] = true;

  return getRuleStr(rules);
};

const stringRule = (fieldName, optional = false) => {
  const rules = {
    string: true,
    required: optional,
  };

  return getRuleStr(rules);
};

const passwordRule = () => {
  const rules = {
    string: true,
    required: true,
    [`min:${CONSTANT.PASSWORD_MIN_LENGTH}`]: true,
    [`max:${CONSTANT.PASSWORD_MAX_LENGTH}`]: true,
    "regex:/^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g": true,
  };

  return getRuleStr(rules);
};

const stringTypeCheck = [
  "accessoryTypeType",
  function (value, requirement, attribute) {
    return typeof value === "number";
  },
  `The :attribute need to number type`,
];

Validator.register(
  "accessoryType",
  function (value, requirement, attribute) {
    return _.values(CONSTANT.Accessory.Type).includes(value);
  },
  `The :attribute need to be one of ${_.values(CONSTANT.Accessory.Type).join(
    ", "
  )}`
);

Validator.register(...stringTypeCheck);

const accessoryTypeRule = () => {
  const rules = {
    numeric: true,
    required: true,
    accessoryType: true,
    accessoryTypeType: true,
  };

  return getRuleStr(rules);
};

const emailRule = (optional = false) => {
  const rules = {
    email: true,
    required: optional,
  };

  return getRuleStr(rules);
};

const booleanRule = (optional = false) => {
  // boolean:
  // The field under validation must be a boolean value of the form true, false, 0, 1, 'true', 'false', '0', '1',

  const rules = {
    boolean: true,
    required: optional,
  };

  return getRuleStr(rules);
};

let rules = {
  name: nameRule(true),
  mac: macRule(),
  mesh: stringRule("meshUuid", true),
  password: passwordRule(),
  accessoryType: accessoryTypeRule(),
  email: emailRule(),
  status: booleanRule(), // based on the docs, 'true' / 1 is treat as boolean also
};

const errMsg = {
  "string.mesh": '":attribute" should be a valid string',
  "regex.mac": '"mac" should be a valid MAC address',
  "regex.password": `"password" should be between ${CONSTANT.PASSWORD_MIN_LENGTH} and ${CONSTANT.PASSWORD_MAX_LENGTH} characters long and contain at least ${CONSTANT.PASSWORD_UPPERCASE_MIN} capital letter and at least ${CONSTANT.PASSWORD_NUMBERS_MIN} digit`,
};

let validation = new Validator(data, rules, errMsg);
validation.setAttributeNames({
  mesh: "meshUuid",
  accessoryType: "accessoryTypeCustom",
});

let passes = validation.passes();

log(chalk.blue("--- Result ---"));
log("Is pass? ", passes);

// Error messages
log(chalk.red("--- Error msg ---"));
log(validation.errors.all());
log(validation.errorCount);
