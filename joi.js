import Joi from "joi";
import chalk from "chalk";
import _ from "lodash";
import data from "./data.js";
import * as CONSTANT from "./constant.js";

const log = console.log;
const prefs = {
  abortEarly: false,
  convert: false,
};

const setIsRequired = (rule, isRequired) =>
  isRequired ? rule.required() : rule;

const nameRule = (isRequired = false) => {
  let rule = Joi.string().max(CONSTANT.NAME_MAX_LENGTH);

  return setIsRequired(rule, isRequired);
};

const macRule = (isRequired = false) => {
  const pattern = `^[0-9a-f]*$`;
  const regex = new RegExp(pattern);
  const rule = Joi.string().regex(regex).length(CONSTANT.MAC_ADDRESS_LENGTH);

  return setIsRequired(rule, isRequired);
};

const stringRule = (fieldName, isRequired = false) => {
  const rule = Joi.string().label(fieldName);

  return setIsRequired(rule, isRequired);
};

const passwordRule = (isRequired = false) => {
  let pattern = `^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]*$`;
  const regex = new RegExp(pattern);
  const errMsg = `"password" should be between ${CONSTANT.PASSWORD_MIN_LENGTH} and ${CONSTANT.PASSWORD_MAX_LENGTH} characters long and contain at least ${CONSTANT.PASSWORD_UPPERCASE_MIN} capital letter and at least ${CONSTANT.PASSWORD_NUMBERS_MIN} digit`;
  const rule = Joi.string()
    .regex(regex, "Password")
    .min(CONSTANT.PASSWORD_MIN_LENGTH)
    .max(CONSTANT.PASSWORD_MAX_LENGTH)
    .messages({
      "string.pattern.name": errMsg,
    });

  return setIsRequired(rule, isRequired);
};

const accessoryTypeRule = (isRequired = false) => {
  const TYPE = _.values(CONSTANT.Accessory.Type);
  const errMsg = `The accessoryTypeCustom need to be one of ${TYPE.join(", ")}`;
  const rule = Joi.number()
    .valid(...TYPE)
    .messages({
      "any.only": errMsg,
    });

  return setIsRequired(rule, isRequired);
};

const emailRule = (isRequired = false) => {
  const rule = Joi.string().email();

  return setIsRequired(rule, isRequired);
};

const booleanRule = (isRequired = false) => {
  const rule = Joi.boolean();

  return setIsRequired(rule, isRequired);
};

const schema = Joi.object({
  name: nameRule(),
  mac: macRule(),
  mesh: stringRule("meshUuid", true),
  password: passwordRule(),
  accessoryType: accessoryTypeRule(),
  email: emailRule(),
  status: booleanRule(),
}).prefs(prefs);

try {
  const value = await schema.validateAsync(data);

  log(chalk.blue("--- Result ---"));
  log("Is pass? ", !!value);
} catch (err) {
  log(chalk.red("--- Errors ---"));
  log(err.details);
}